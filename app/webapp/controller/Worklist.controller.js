sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Dialog",
], function (BaseController, JSONModel, formatter, Filter, FilterOperator, Dialog) {
	"use strict";

	return BaseController.extend("my.bookshop.app.controller.Worklist", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
			var oViewModel;

			// keeps the search state
			this._aTableSearchState = [];

			// Model used to manipulate control states
			oViewModel = new JSONModel({
				worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
				shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
				shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
				shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
				tableNoDataText : this.getResourceBundle().getText("tableNoDataText")
			});
			this.setModel(oViewModel, "worklistView");

		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */

		/**
		 * Triggered by the table's 'updateFinished' event: after new table
		 * data is available, this handler method updates the table counter.
		 * This should only happen if the update was successful, which is
		 * why this handler is attached to 'updateFinished' and not to the
		 * table's list binding's 'dataReceived' method.
		 * @param {sap.ui.base.Event} oEvent the update finished event
		 * @public
		 */
		onUpdateFinished : function (oEvent) {
			// update the worklist's object counter after the table update
			var sTitle,
				oTable = oEvent.getSource(),
				iTotalItems = oEvent.getParameter("total");
			// only update the counter if the length is final and
			// the table is not empty
			if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
				sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
			} else {
				sTitle = this.getResourceBundle().getText("worklistTableTitle");
			}
			this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
		},

		/**
		 * Event handler when a table item gets pressed
		 * @param {sap.ui.base.Event} oEvent the table selectionChange event
		 * @public
		 */
		onPress : function (oEvent) {
			// The source is the list item that got pressed
			this._showObject(oEvent.getSource());
		},

		/**
		 * Event handler for navigating back.
		 * Navigate back in the browser history
		 * @public
		 */
		onNavBack : function() {
			// eslint-disable-next-line sap-no-history-manipulation
			history.go(-1);
		},


		onSearch : function (oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
			} else {
				var aTableSearchState = [];
				var sQuery = oEvent.getParameter("query");

				if (sQuery && sQuery.length > 0) {
					aTableSearchState = [new Filter("title", FilterOperator.Contains, sQuery)];
				}
				this._applySearch(aTableSearchState);
			}

		},

		/**
		 * Event handler for refresh event. Keeps filter, sort
		 * and group settings and refreshes the list binding.
		 * @public
		 */
		onRefresh : function () {
			var oTable = this.byId("table");
			oTable.getBinding("items").refresh();
		},

		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Shows the selected item on the object page
		 * On phones a additional history entry is created
		 * @param {sap.m.ObjectListItem} oItem selected Item
		 * @private
		 */
		_showObject : function (oItem) {
			var that = this;

			oItem.getBindingContext().requestCanonicalPath().then(function (sObjectPath) {
				that.getRouter().navTo("object", {
					objectId_Old: oItem.getBindingContext().getProperty("ID"),
					objectId : sObjectPath.slice("/Books".length) // /Products(3)->(3)
				});
			});
		},

		/**
		 * Internal helper method to apply both filter and search state together on the list binding
		 * @param {sap.ui.model.Filter[]} aTableSearchState An array of filters for the search
		 * @private
		 */
		_applySearch: function(aTableSearchState) {
			var oTable = this.byId("table"),
				oViewModel = this.getModel("worklistView");
			oTable.getBinding("items").filter(aTableSearchState, "Application");
			// changes the noDataText of the list in case there are no filter results
			if (aTableSearchState.length !== 0) {
				oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
			}
		},
		
		_openEditableFields: function(oItem, oFlag) {
			var oEditableCells = oItem.getCells();
			$(oEditableCells).each(function(i) {
    			var oEditableCell = oEditableCells[i];
    			var oMetaData = oEditableCell.getMetadata();
    			var oElement = oMetaData.getElementName();
	    		if (oElement == "sap.m.Input") {
	    			oEditableCell.setEditable(oFlag);
	    		}
			});
        },
        
        //*****************************************************************************************************************************
        
        onBookSelect : function (oEvent) {
				var oUIModel = this.getModel("worklistView");
				oUIModel.setProperty("/bBookValue", oEvent.getParameter("listItem"));
		},
		
		onChangeBook : function () {
	      	var oUIModel = this.getModel("worklistView");
	    	this._openEditableFields(oUIModel.getProperty("/bBookValue"), true);
		},
		
        
        onCancelBookListChanges : function (oEvent) {

			var oModel = this.getOwnerComponent().getModel();
			var vModelPendingChanges = oModel.hasPendingChanges();
			var oView = this;
			
			if (vModelPendingChanges === true) {
				var self = this;
				var dialog = this.getModel("worklistView").getData().openDialog = new Dialog({
					title: 'Cancel Book Changes',
					type: 'Message',
					content: new sap.m.Label({ text: 'Would you like to keep your changes?' }),
					beginButton: new sap.m.Button({
						text: 'Keep Changes',
						enabled: true,
						press: function (){
							self.onKeepBookChanges();
						}
					}),
					endButton: new sap.m.Button({
						text: 'Discard',
						press: function () {
							oModel.resetChanges('bookGroup');
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});
	
				dialog.open();
			} 
			
			
			this.getView().byId("booksTable").removeSelections(true);
			
			var oUIModel = this.getModel("worklistView");
			this._openEditableFields(oUIModel.getProperty("/bBookValue"), false);
		},
		
		onKeepBookChanges : function (){
			this.onSaveBook();
			this.getModel("worklistView").getData().openDialog.close();
		},
		
		onSaveBook : function () {
			var oModel = this.getOwnerComponent().getModel();	
			var oView = this;
		  
			let fnError = (oError) => { 
				sap.m.MessageToast.show("Update failed") 
			}
			
			let fnSuccess = () => {
				this.refreshTableItems("booksTable");
				
				oView.getView().byId("booksTable").removeSelections(true);
				var oUIModel = oView.getModel("worklistView");
				oView._openEditableFields(oUIModel.getProperty("/bBookValue"), false);
			}

			oModel.submitBatch("bookGroup").then(fnSuccess, fnError);
		},
		
		onCreateBook: function () {
			var that = this; 
			var oModel = this.getOwnerComponent().getModel();
			var dialog = new Dialog({
				title: 'Create Book',
				content: [
					new sap.ui.layout.form.SimpleForm({
						editable: true,
						layout: 'ResponsiveGridLayout',
						content: [
							new sap.m.Label({ text: 'Book Id', labelFor: 'idBookIdCreate', required: true }),
							new sap.m.Input({ id: 'idBookIdCreate', required: true, type: sap.m.InputType.Number}),
							new sap.m.Label({ text: 'Book', labelFor: 'idBookCreate', required: true }),
							new sap.m.Input({ id: 'idBookCreate', required: true}),
							new sap.m.Label({ text: 'Author', labelFor: 'idAuthorCreate', required: true }),
							new sap.m.Input({ id: 'idAuthorCreate', required: true}),
							new sap.m.Label({ text: 'Stock', labelFor: 'idStockCreate', required: true }),
							new sap.m.Input({ id: 'idStockCreate', required: true})
						]
					})
				],
				beginButton: new sap.m.Button({
					text: 'Save',
					enabled: true,
					press: (oEvent)=> {
						
						var bookBinding = that.byId('booksTable').getBinding('items');
						var oContext = bookBinding.create({
							'ID':  parseInt(sap.ui.getCore().byId('idBookIdCreate').getValue()),
							'title': sap.ui.getCore().byId('idBookCreate').getValue(),
							'author_ID': parseInt(sap.ui.getCore().byId('idAuthorCreate').getValue()),
							'stock': parseInt(sap.ui.getCore().byId('idStockCreate').getValue())
						});
						
						oModel.submitBatch('bookGroup').then( 
							()=>{ 
								this.refreshTableItems("booksTable");
							},
							(oError)=>{ } 
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Cancel',
					press: ()=> { dialog.close() }
				}),
				afterClose: ()=> { dialog.destroy() }
			});
			dialog.open();
		},
		
		onDeleteBook : function (oEvent) {
			
			var	oTable = this.byId("booksTable"),
			oBookItem = oTable.getSelectedItem();
			var oBookName = oBookItem.getBindingContext().getProperty("title");
				
			var	oBookContext = oTable.getSelectedItem().getBindingContext();
			var oModel = this.getOwnerComponent().getModel();
			var self = this;

			var dialog = new Dialog({
				title: 'Delete Book',
				type: 'Message',
				content: [ new sap.m.Label({ text: 'Are you sure you want to delete Book ' + oBookName})],
				beginButton: new sap.m.Button({
					text: 'Delete',
					enabled: true,
					press: function () {
						oBookContext.delete(oBookContext.getModel().getGroupId()).then(
							function() { 
										sap.m.MessageToast.show("Delete successful") 
							}.bind(this),
							function(oError) { 
												sap.m.MessageToast.show("Delete failed") 
							}.bind(this)
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Close',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});
			
			dialog.open();
			
		},
        
        //*****************************************************************************************************************************
        
        onAuthorSelect : function (oEvent) {
				var oUIModel = this.getModel("worklistView");
				oUIModel.setProperty("/bAuthorValue", oEvent.getParameter("listItem"));
		},
		
		onChangeAuthor : function () {
	      	var oUIModel = this.getModel("worklistView");
	    	this._openEditableFields(oUIModel.getProperty("/bAuthorValue"), true);
		},
		
        
        onCancelAuthorListChanges : function (oEvent) {

			var oModel = this.getOwnerComponent().getModel();
			var vModelPendingChanges = oModel.hasPendingChanges();
			var oView = this;
			
			if (vModelPendingChanges === true) {
				var self = this;
				var dialog = this.getModel("worklistView").getData().openDialog = new Dialog({
					title: 'Cancel Author Changes',
					type: 'Message',
					content: new sap.m.Label({ text: 'Would you like to keep your changes?' }),
					beginButton: new sap.m.Button({
						text: 'Keep Changes',
						enabled: true,
						press: function (){
							self.onKeepAuthorChanges();
						}
					}),
					endButton: new sap.m.Button({
						text: 'Discard',
						press: function () {
							oModel.resetChanges('authorGroup');
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});
	
				dialog.open();
			} 
			
			
			this.getView().byId("authorsTable").removeSelections(true);
			
			var oUIModel = this.getModel("worklistView");
			this._openEditableFields(oUIModel.getProperty("/bAuthorValue"), false);
		},
		
		onKeepAuthorChanges : function (){
			this.onSaveAuthor();
			this.getModel("worklistView").getData().openDialog.close();
		},
		
		onSaveAuthor : function () {
			var oModel = this.getOwnerComponent().getModel();	
			var oView = this;
		  
			let fnError = (oError) => { 
				sap.m.MessageToast.show("Update failed") 
			}
			
			let fnSuccess = () => {
				this.refreshTableItems("authorsTable");
				
				oView.getView().byId("authorsTable").removeSelections(true);
				var oUIModel = oView.getModel("worklistView");
				oView._openEditableFields(oUIModel.getProperty("/bAuthorValue"), false);
			}

			oModel.submitBatch("authorGroup").then(fnSuccess, fnError);
		},
		
		onCreateAuthor: function () {
			var that = this; 
			var oModel = this.getOwnerComponent().getModel();
			var dialog = new Dialog({
				title: 'Create Author',
				content: [
					new sap.ui.layout.form.SimpleForm({
						editable: true,
						layout: 'ResponsiveGridLayout',
						content: [
							new sap.m.Label({ text: 'Author Id', labelFor: 'idAuthorIdCreate', required: true }),
							new sap.m.Input({ id: 'idAuthorIdCreate', required: true, type: sap.m.InputType.Number}),
							new sap.m.Label({ text: 'Author Name', labelFor: 'idNameCreate', required: true }),
							new sap.m.Input({ id: 'idNameCreate', required: true}),
						]
					})
				],
				beginButton: new sap.m.Button({
					text: 'Save',
					enabled: true,
					press: (oEvent)=> {
						
						var oBinding = that.byId('authorsTable').getBinding('items');
						var oContext = oBinding.create({
							'ID':  parseInt(sap.ui.getCore().byId('idAuthorIdCreate').getValue()),
							'name': sap.ui.getCore().byId('idNameCreate').getValue(),
						});
						
						oModel.submitBatch('authorGroup').then( 
							()=>{ 
								this.refreshTableItems("authorsTable");
							},
							(oError)=>{ } 
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Cancel',
					press: ()=> { dialog.close() }
				}),
				afterClose: ()=> { dialog.destroy() }
			});
			dialog.open();
		},
		
		onDeleteAuthor : function (oEvent) {
			
			var	oTable = this.byId("authorsTable"),
			oItem = oTable.getSelectedItem();
			var oName = oItem.getBindingContext().getProperty("name");
				
			var	oContext = oTable.getSelectedItem().getBindingContext();
			var oModel = this.getOwnerComponent().getModel();
			var self = this;

			var dialog = new Dialog({
				title: 'Delete Author',
				type: 'Message',
				content: [ new sap.m.Label({ text: 'Are you sure you want to delete Author ' + oName})],
				beginButton: new sap.m.Button({
					text: 'Delete',
					enabled: true,
					press: function () {
						oContext.delete(oContext.getModel().getGroupId()).then(
							function() { 
										sap.m.MessageToast.show("Delete successful") 
							}.bind(this),
							function(oError) { 
												sap.m.MessageToast.show("Delete failed") 
							}.bind(this)
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Close',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});
			
			dialog.open();
			
		},
        
        //*****************************************************************************************************************************
        
        onOrderSelect : function (oEvent) {
				var oUIModel = this.getModel("worklistView");
				oUIModel.setProperty("/bOrderValue", oEvent.getParameter("listItem"));
		},
		
		onChangeOrder : function () {
	      	var oUIModel = this.getModel("worklistView");
	    	this._openEditableFields(oUIModel.getProperty("/bOrderValue"), true);
		},
		
        
        onCancelOrderListChanges : function (oEvent) {

			var oModel = this.getOwnerComponent().getModel();
			var vModelPendingChanges = oModel.hasPendingChanges();
			var oView = this;
			
			if (vModelPendingChanges === true) {
				var self = this;
				var dialog = this.getModel("worklistView").getData().openDialog = new Dialog({
					title: 'Cancel Author Changes',
					type: 'Message',
					content: new sap.m.Label({ text: 'Would you like to keep your changes?' }),
					beginButton: new sap.m.Button({
						text: 'Keep Changes',
						enabled: true,
						press: function (){
							self.onKeepOrderChanges();
						}
					}),
					endButton: new sap.m.Button({
						text: 'Discard',
						press: function () {
							oModel.resetChanges('authorGroup');
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});
	
				dialog.open();
			} 
			
			
			this.getView().byId("ordersTable").removeSelections(true);
			
			var oUIModel = this.getModel("worklistView");
			this._openEditableFields(oUIModel.getProperty("/bOrderValue"), false);
		},
		
		onKeepOrderChanges : function (){
			this.onSaveAuthor();
			this.getModel("worklistView").getData().openDialog.close();
		},
		
		onSaveOrder : function () {
			var oModel = this.getOwnerComponent().getModel();	
			var oView = this;
		  
			let fnError = (oError) => { 
				sap.m.MessageToast.show("Update failed") 
			}
			
			let fnSuccess = () => {
				this.refreshTableItems("ordersTable");
				
				oView.getView().byId("ordersTable").removeSelections(true);
				var oUIModel = oView.getModel("worklistView");
				oView._openEditableFields(oUIModel.getProperty("/bOrderValue"), false);
			}

			oModel.submitBatch("orderGroup").then(fnSuccess, fnError);
		},
		
		onCreateOrder: function () {
			var that = this; 
			var oModel = this.getOwnerComponent().getModel();
			var dialog = new Dialog({
				title: 'Create Order',
				content: [
					new sap.ui.layout.form.SimpleForm({
						editable: true,
						layout: 'ResponsiveGridLayout',
						content: [
							new sap.m.Label({ text: 'Book Id', labelFor: 'idBookIdCreate', required: true }),
							new sap.m.Input({ id: 'idBookIdCreate', required: true, type: sap.m.InputType.Number}),
							new sap.m.Label({ text: 'Amount', labelFor: 'idAmountCreate', required: true }),
							new sap.m.Input({ id: 'idAmountCreate', required: true}),
						]
					})
				],
				beginButton: new sap.m.Button({
					text: 'Save',
					enabled: true,
					press: (oEvent)=> {
						
						var oBinding = that.byId('ordersTable').getBinding('items');
						var oContext = oBinding.create({
							'book_ID':  parseInt(sap.ui.getCore().byId('idBookIdCreate').getValue()),
							'amount': parseInt(sap.ui.getCore().byId('idAmountCreate').getValue()),
						});
						
						oModel.submitBatch('orderGroup').then( 
							()=>{ 
								this.refreshTableItems("ordersTable");
							},
							(oError)=>{ } 
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Cancel',
					press: ()=> { dialog.close() }
				}),
				afterClose: ()=> { dialog.destroy() }
			});
			dialog.open();
		},
		
		onDeleteOrder : function (oEvent) {
			
			var	oTable = this.byId("ordersTable"),
			oItem = oTable.getSelectedItem();
			var oName = oItem.getBindingContext().getProperty("ID");
				
			var	oContext = oTable.getSelectedItem().getBindingContext();
			var oModel = this.getOwnerComponent().getModel();
			var self = this;

			var dialog = new Dialog({
				title: 'Delete Author',
				type: 'Message',
				content: [ new sap.m.Label({ text: 'Are you sure you want to delete Order ' + oName})],
				beginButton: new sap.m.Button({
					text: 'Delete',
					enabled: true,
					press: function () {
						oContext.delete(oContext.getModel().getGroupId()).then(
							function() { 
										sap.m.MessageToast.show("Delete successful") 
							}.bind(this),
							function(oError) { 
												sap.m.MessageToast.show("Delete failed") 
							}.bind(this)
						);
						dialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: 'Close',
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});
			
			dialog.open();
			
		},

	});
});